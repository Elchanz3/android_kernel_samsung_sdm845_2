#!/bin/bash
# CHANZ-KERNEL-BUILD

# Variables
DIR=`readlink -f .`
OUT_DIR=$DIR/out
PARENT_DIR=`readlink -f ${DIR}/..`

export PLATFORM_VERSION=10
export ANDROID_MAJOR_VERSION=q

# Download Linaro GCC toolchain if not downloaded yet
if [ ! -f "$HOME/tc/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu.tar.xz" ]; then
  echo "Downloading Linaro GCC toolchain..."
  wget -P "$HOME/tc" https://releases.linaro.org/components/toolchain/binaries/6.5-2018.12/aarch64-linux-gnu/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu.tar.xz
  echo "Linaro GCC toolchain downloaded."
fi

# Extract and rename Linaro GCC toolchain if not extracted yet
export TC_DIR="$HOME/tc"
export GCC_TOOLCHAIN_DIR="$TC_DIR/gcc-linaro-6.5.0/bin"
export BUILD_CROSS_COMPILE="$GCC_TOOLCHAIN_DIR/aarch64-linux-gnu-"
export VARIANT=crownqltechn

if [ ! -d "$TC_DIR/gcc-linaro-6.5.0" ]; then
  echo "Extracting Linaro GCC toolchain..."
  mkdir -p "$TC_DIR"
  tar -xf "$HOME/tc/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu.tar.xz" -C "$TC_DIR"
  mv "$TC_DIR/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu" "$TC_DIR/gcc-linaro-6.5.0"
  echo "Linaro GCC toolchain extracted."
fi
CC_TOOLCHAIN_DIR="$TC_DIR/gcc-linaro-6.5.0/bin"
BUILD_CROSS_COMPILE="$GCC_TOOLCHAIN_DIR/aarch64-linux-gnu-"

if [ ! -d "$TC_DIR/gcc-linaro-6.5.0" ]; then
  echo "Extracting Linaro GCC toolchain..."
  mkdir -p "$TC_DIR"
  tar -xf $HOME/tc/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu.tar.xz -C "$TC_DIR"
  mv "$TC_DIR/gcc-linaro-6.5.0-2018.12-x86_64_aarch64-linux-gnu" "$TC_DIR/gcc-linaro-6.5.0"
  echo "Linaro GCC toolchain extracted."
fi

echo "auto installing necessary dependencies please wait."

sudo apt-get update && sudo apt-get upgrade && sudo apt-get install git ccache automake lzop bison gperf build-essential zip curl zlib1g-dev libxml2-utils bzip2 libbz2-dev libbz2-1.0 libghc-bzlib-dev squashfs-tools pngcrush schedtool dpkg-dev liblz4-tool make optipng bc gcc-aarch64-linux-gnu clang -y

# BUILD_CROSS_COMPILE=$HOME/gcc-linaro-6.5.0/bin/aarch64-linux-gnu-
export KERNEL_MAKE_ENV="LOCALVERSION=_HyperForceKernel-v1.1.3"
export KERNEL_ZIP_VERSION=HyperForceKernel-v1.1.3
# DEFCONFIGS
D1=chanz_crownqltechn_open_defconfig
D2=chanz_star2qltechn_open_defconfig
D3=chanz_starqltechn_open_defconfig

# LOG FILE NAME
LOG_FILE=compilation-HFK.log

make -j$(nproc) -C $(pwd) O=$(pwd)/out $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE chanz_crownqltechn_defconfig

DATE_START=$(date +"%s")
 
make -j$(nproc) -C $(pwd) O=$(pwd)/out $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE 2>&1 |tee ../$LOG_FILE

-e $OUT_DIR/arch/arm64/boot/Image.gz ] && cp $OUT_DIR/arch/arm64/boot/Image.gz $OUT_DIR/Image.gz

if [ -e $OUT_DIR/arch/arm64/boot/Image.gz-dtb ]; then
cp $OUT_DIR/arch/arm64/boot/Image.gz-dtb $OUT_DIR/Image.gz-dtb

  DATE_END=$(date +"%s")
    DIFF=$(($DATE_END - $DATE_START))

echo "Time wasted: $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds."

echo "done.."

if [ ! -d AnyKernel3 ]; then
    pause 'clone AnyKernel3 - Flashable Zip Template'
    git clone https://github.com/Elchanz3/AnyKernel3 /AnyKernel3
  fi
  variant
  if [ -e $OUT_DIR/arch/arm64/boot/Image.gz-dtb ]; then
    cd AnyKernel3

    cp $OUT_DIR/arch/arm64/boot/Image.gz-dtb zImage
    sed -i "s/ExampleKernel by osm0sis/${VARIANT} kernel by chanz22/g" anykernel.sh
    sed -i "s/=maguro/=${VARIANT}/g" anykernel.sh
    sed -i "s/=toroplus/=/g" anykernel.sh
    sed -i "s/=toro/=/g" anykernel.sh
    sed -i "s/=tuna/=/g" anykernel.sh
    sed -i "s/backup_file/#backup_file/g" anykernel.sh
    sed -i "s/replace_string/#replace_string/g" anykernel.sh
    sed -i "s/insert_line/#insert_line/g" anykernel.sh
    sed -i "s/append_file/#append_file/g" anykernel.sh
    sed -i "s/patch_fstab/#patch_fstab/g" anykernel.sh
    sed -i "s/dump_boot/split_boot/g" anykernel.sh
    sed -i "s/write_boot/flash_boot/g" anykernel.sh
    zip -r9 ${VARIANT}_${KERNEL_ZIP_VERSION}.zip * -x .git README.md *placeholder
    cd $DIR
